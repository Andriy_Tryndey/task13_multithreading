package com.andriy.view;

import com.andriy.model.Fabonacci;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;

public class MyView {
    private volatile long A = 0;
    private static int x = 0;
    private Object sync = new Object();
    private static Scanner sc = new Scanner(System.in);
    private Random random = new Random(20);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView(){
        menu = new LinkedHashMap<>();

        menu.put("1", "1. “ping-pong” program");
        menu.put("2", "2. Create a task that produces a sequence of n Fibonacci numbers, where n is provided to the constructor of the task. Create a number of these tasks and drive them using threads. ");
        menu.put("3", "3. Repeat previous exercise using the different types of executors.");
        menu.put("4", "4. Modify Exercise 2 so that the task is a Callable that sums the values of all the Fibonacci numbers.");
        menu.put("5", "5. Create a task that sleeps for a random amount of time between 1 and 10 seconds, then displays its sleep time and exits.");
        menu.put("6", "6. Create a class with three methods containing critical sections that all synchronize on the same object.");
        menu.put("7", "7. Write program in which two tasks use a pipe to communicate. ");
        menu.put("Q", "exit");

        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
    }

    /**
     * create pin-pong with two theads
     */
    private void pressButton1() {
        Thread t1 = new Thread(()->{synchronized (sync){
        for (int i = 1; i <= 10; i++){
        try {
            sync.wait();
        } catch (InterruptedException e) {e.printStackTrace();}
        A++;
            System.out.println("A = " + A + " - " + Thread.currentThread().getName());
        sync.notify();
        }
            System.out.println("finish" + Thread.currentThread().getName());
        }});
        Thread t2 = new Thread(()->{synchronized (sync){
        for (int i = 1; i <= 10; i++){
        sync.notify();
        try {
            sync.wait();
        } catch (InterruptedException e) {e.printStackTrace();}
        A++;
            System.out.println("A = " + A + " - " + Thread.currentThread().getName());
        }
            System.out.println("finish - " + Thread.currentThread().getName());
        }});
        System.out.println("1 - " + LocalDateTime.now());
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {e.printStackTrace();}
        System.out.println("2 - " + LocalDateTime.now());
        System.out.println("A = " + A);
    }

    /**
     * synchronization between threads
     */
    private void pressButton2() {
        Thread t1 = new Thread(()->{synchronized (sync){
        for (int i = 1; i <= 3; i++){
            try {
                sync.wait(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new Fabonacci(random);
            System.out.println(Thread.currentThread().getName());
        }
        sync.notify();
            System.out.println("Finissh first thread!!!");
        }
        });
        Thread t2 = new Thread(()->{synchronized (sync){
        for (int i = 1; i <= 2; i++){
            sync.notify();
            try {
                sync.wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new Fabonacci(random);
            System.out.println(Thread.currentThread().getName());}
        }
            System.out.println("Finissh second thread!!!");
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void pressButton3() {

    }

    /**
     * Method that create tasks
     * @return
     */
    public Integer sum(){
        int sum = 0;
        Fabonacci fabonacci = new Fabonacci(random);
        System.out.println(fabonacci);
        for (Integer i : fabonacci.list) {
            sum = sum + i;
        }
        return sum;
    }

    /**
     * using callable interfase
     */
    private void pressButton4() {
        ExecutorService executor = Executors.newWorkStealingPool();
        List<Callable<Integer>> callables = Arrays.asList(()->sum(), ()->sum(), ()->sum());
        try {
            executor.invokeAll(callables).stream().map(future->{
                try {
                    return future.get();
                } catch (Exception e) {
                    throw new IllegalStateException();
                }
            }).forEach(System.out::println);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void pressButton5() {

    }

    private void pressButton6() {

    }

    private void pressButton7() {

    }

    public void show(){
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Select menu point :");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){}
        }while (!keyMenu.equals("Q"));
    }

    public void outputMenu() {
        System.out.println("\nMenu");
        for (String str : menu.values()){
            System.out.println(str);
        }
    }
}
